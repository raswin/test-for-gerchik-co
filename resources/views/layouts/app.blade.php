<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Users</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('template/bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css')}}">
        <!-- DataTables -->
        <link rel="stylesheet" href="{{asset('template/plugins/datatables/dataTables.bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('template/plugins/select2/select2.css')}}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('template/css/AdminLTE.css')}}">
        <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset('template/css/skins/_all-skins.min.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <script src="{{url('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js')}}"></script>
        <script src="{{url('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="{{ url('/') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>G</b>C</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Gerchik & Co</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">NAVIGATION</li>
                    <li class='active'>
                        <a href="{{ url('/') }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            <span>Users</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <!-- Main content -->
            <section class="content" style="overflow: auto;">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="{{asset('template/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{asset('template/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('template/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('template/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('template/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('template/plugins/select2/select2.min.js')}}"></script>

    <!-- FastClick -->
    <script src="{{asset('template/plugins/fastclick/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template/js/app.min.js')}}"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('template/js/demo.js')}}"></script>
    <script src="{{asset('template/js/jquery-ui.min.js') }}"></script>
    <script src="{{asset('template/js/jquery.ui.touch-punch.min.js') }}"></script>
    <!-- page script -->
    <script>
        $('#users').DataTable({
            "paging": true,
            "lengthChange": true,
            "stateSave": true,
            "iDisplayLength": 25,
            "searching": true,
            "order":[[0, 'asc']],
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true
        });
        $(document).ready(function(){
            $(".user").submit(function() {
                var form_data = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "{{ url('/user') }}",
                    data: form_data,
                    success: function(data) {
                        if($.isEmptyObject(data.error)){
                            $('.print-error-msg').hide();
                            alert(data.success);
                        }else{
                            printErrorMsg(data.error);
                        }
                    }
                });
                return false;
            });

            function printErrorMsg (msg) {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").show();
                $.each( msg, function( key, value ) {
                    $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                });
            }
        });
    </script>
    </body>
</html>
