@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-js">
                <div class="box-header with-border">
                    <h3 class="box-title">Add user</h3>
                </div>
                <div class="box-body">
                    <form action="{{ url('/user') }}" method="POST" class="user">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="usr_name">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="usr_email">
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control" name="usr_address">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-js">
                <div class="box-header with-border">
                    <h3 class="box-title">Add account</h3>
                </div>
                <div class="box-body">
                    <form action="{{ url('/account') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Select user</label>
                            <select name="user_id" class="form-control">
                                @foreach($users->where('accounts', NULL) as $user)
                                    <option value="{{ $user->id }}" @if(old('user_id') == $user->id) selected @endif>{{ $user->usr_email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Account</label>
                            <input type="number" class="form-control" name="account" value="{{ old('account') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" @if(count($users->where('accounts', NULL)) == 0) disabled @endif class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <table id="users" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Accounts</th>
            <th>Created</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->usr_name }}</td>
                <td>{{ $user->usr_email }}</td>
                <td>{{ $user->usr_address }}</td>
                <td>
                    @if($user->accounts)
                        {{ $user->accounts->account }}
                    @else
                        -
                    @endif
                </td>
                <td>{{ $user->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

