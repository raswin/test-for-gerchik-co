<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'usr_name',
        'usr_email',
        'usr_address'
    ];

    public function accounts(){
        return $this->hasOne(Accounts::class, 'user_id', 'id');
    }
}
