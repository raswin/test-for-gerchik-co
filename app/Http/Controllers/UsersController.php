<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Accounts;
use Validator;

class UsersController extends Controller
{
    public function getAll()
    {
        $users = Users::with('accounts')->get();
        return view('pages/list', ['users' => $users]);
    }

    public function createUser( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'usr_name' => 'required|max:255',
            'usr_email' => 'required|email|unique:users|max:255',
            'usr_address' => 'required|max:255',
        ]);
        if ($validator->passes()) {
            Users::create([
               'usr_name' => $request->input('usr_name'),
               'usr_email' => $request->input('usr_email'),
               'usr_address' => $request->input('usr_address')
            ]);
            return response()->json(['success' => 'Added new user.']);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }

    public function createAccount( Request $request )
    {
        $validation = Validator::make($request->all(), [
                'user_id' => 'required|exists:users,id',
                'account' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return back()->withErrors($validation)->withInput();
        } else {
            Accounts::create([
              'user_id' => $request->input('user_id'),
              'account' => $request->input('account')
            ]);
            return back();
        }

    }
}
